##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x32
ProjectName            :=joystick
ConfigurationName      :=Debug_x32
WorkspacePath          := "/work/Development/orx-projects/joystick/build/linux/codelite"
ProjectPath            := "/work/Development/orx-projects/joystick/build/linux/codelite"
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=root
Date                   :=14/02/18
CodeLitePath           :="/root/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/joystickd
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="joystick.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -m32 -L/usr/lib32 -Wl,-rpath ./ -Wl,--export-dynamic
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include $(IncludeSwitch)$(ORX)/include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd $(LibrarySwitch)dl $(LibrarySwitch)m $(LibrarySwitch)rt 
ArLibs                 :=  "orxd" "dl" "m" "rt" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -ffast-math -g -msse2 -m32 -fno-exceptions -Wno-unused-function $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -m32 -fno-exceptions -Wno-unused-function $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_joystick.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	$(shell [ -f /work/Development/orx-projects/orx/code/lib/dynamic/liborx.so ] && cp -f /work/Development/orx-projects/orx/code/lib/dynamic/liborx*.so ../../../bin)
	@echo Done

MakeIntermediateDirs:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)


$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_joystick.cpp$(ObjectSuffix): ../../../src/joystick.cpp $(IntermediateDirectory)/src_joystick.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/work/Development/orx-projects/joystick/src/joystick.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_joystick.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_joystick.cpp$(DependSuffix): ../../../src/joystick.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_joystick.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_joystick.cpp$(DependSuffix) -MM "../../../src/joystick.cpp"

$(IntermediateDirectory)/src_joystick.cpp$(PreprocessSuffix): ../../../src/joystick.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_joystick.cpp$(PreprocessSuffix) "../../../src/joystick.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/


