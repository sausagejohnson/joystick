/* Orx - Portable Game Engine
 *
 * Copyright (c) 2008-2017 Orx-Project
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *    distribution.
 */

/**
 * @file joystick.cpp
 * @date 17/05/2017
 * @author sausage@zeta.org.au
 *
 * Orx Game template
 */


#include "orx.h"

orxBOOL remapMode = orxFALSE;

/*
 * This is a basic C++ template to quickly and easily get started with a project or tutorial.
 */


/** Initializes your game
 */
orxSTATUS orxFASTCALL Init()
{
    /* Displays a small hint in console */
    orxLOG("\n* This template creates a viewport/camera couple and an object"
    "\n* You can play with the config parameters in ../data/config/joystick.ini"
    "\n* After changing them, relaunch the template to see the changes.");

    /* Creates the viewport */
    orxViewport_CreateFromConfig("Viewport");

    /* Creates the object */
    //orxObject_CreateFromConfig("Object");

    /* Done! */
    return orxSTATUS_SUCCESS;
}

/** Run function, is called every clock cycle
 */
orxSTATUS orxFASTCALL Run()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Should quit? */
    if(orxInput_IsActive("Quit"))
    {
        /* Updates result */
        eResult = orxSTATUS_FAILURE;
    }
	
	if (orxInput_HasBeenActivated("Remap")) {
		orxLOG("Remap mode is on.");
		remapMode = orxTRUE;
	}


	if (remapMode) {

		orxINPUT_TYPE inputType;
		orxENUM buttonID;
		orxFLOAT value;

		//get a button press. This will be the one assigned to 'XButton'
		if (orxInput_GetActiveBinding(&inputType, &buttonID, &value)) {
			if (inputType == orxINPUT_TYPE_JOYSTICK_BUTTON) { //only allow if it's a joystick button press
				orxLOG("Remapping Type: %d ID: %d Value: %f", inputType, buttonID, value); // 4 0 ?
				//do the remap here.

				//Get all inputs and buttonIDs assigned to XButton
				orxINPUT_TYPE inputTypes[orxINPUT_KU32_BINDING_NUMBER];
				orxENUM bindingButtonIDs[orxINPUT_KU32_BINDING_NUMBER];
				orxINPUT_MODE inputModes[orxINPUT_KU32_BINDING_NUMBER];
				orxSTATUS gotListStatus = orxInput_GetBindingList("XButton", inputTypes, bindingButtonIDs, inputModes);

				//unbind all joystick buttons bound to 'XButton'
				for (orxU32 i = 0; i < orxINPUT_KU32_BINDING_NUMBER; i++) {
					if (inputTypes[i] == orxINPUT_TYPE_JOYSTICK_BUTTON) {

						//get the binding index of the 'XButton'
						const orxSTRING stringOut;
						orxU32 bindingIndex;
						orxSTATUS gotInputStatus = orxInput_GetBoundInput(orxINPUT_TYPE_JOYSTICK_BUTTON, bindingButtonIDs[i], orxINPUT_MODE_FULL, 0, &stringOut, &bindingIndex);

						if (gotInputStatus) {
							orxSTATUS unBindSuccess = orxInput_Unbind("XButton", bindingIndex); //remove joystick binding
						}

					}
				}

				orxInput_Bind("XButton", orxINPUT_TYPE_JOYSTICK_BUTTON, _peID, orxINPUT_MODE_FULL, -1);
				orxInput_Save("gamepad.ini");
				orxLOG("Bound to button %d", _peID);

				remapMode = orxFALSE;
			}
		}
	}

	if(orxInput_HasBeenActivated("XButton")){
		orxLOG("X Button");
	}


    /* Done! */
    return eResult;
}

/** Exit function
 */
void orxFASTCALL Exit()
{
    /* Lets Orx clean all our mess automatically. :) */
}

/** Bootstrap function
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Adds a config storage to find the initial config file */
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    /* Done! */
    return eResult;
}

/** Main function
 */
int main(int argc, char **argv)
{
    /* Sets bootstrap function to provide at least one resource storage before loading any config files */
    orxConfig_SetBootstrap(Bootstrap);

    /* Executes a new instance of tutorial */
    orx_Execute(argc, argv, Init, Run, Exit);

    /* Done! */
    return EXIT_SUCCESS;
}
